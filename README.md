# mdlinkc

markdown link collection

---

## license

The project `'mdlinkc'` is licensed under the MIT License.

See [LICENSE](./LICENSE) for more details.

The latest source code can be retrieved from one of several mirrors:

1. [github.com/valera-rozuvan/mdlinkc](https://github.com/valera-rozuvan/mdlinkc)

2. [gitlab.com/valera-rozuvan/mdlinkc](https://gitlab.com/valera-rozuvan/mdlinkc)

3. [git.rozuvan.net/mdlinkc](https://git.rozuvan.net/mdlinkc)

Copyright (c) 2019-2022 [Valera Rozuvan](https://valera.rozuvan.net/)
